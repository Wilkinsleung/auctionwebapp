export class Product {
  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number,
    public desc: string,
    public category: Array<string>
  ) { }
}

export class Comment {
  constructor(
    public id: number,
    public productId: number,
    public timestamp: string,
    public user: string,
    public rating: number,
    public content: string
  ) { }
}


export const products = [
  new Product(1, 'Camellias', 4, 4, 'Glossy green leaves, lovely blossoms, oil, and the invigorating beverage that spurred the American revolution all come from plants in the Camellia family.', ['flowers']),
  new Product(2, 'Amaryllis', 14, 3, 'The stunning amaryllis belladonna is known as Naked Lady in the US. The smooth-textured foliage grows in early summer then dies back.', ['flowers']),
  new Product(3, 'Amazon lily', 10, 5, 'a type of flower, red color', ['flowers']),
  new Product(4, 'Freesia', 9, 1, 'The Freesia flower, a herbaceous member of the Iris family, is regarded as one of the most fragrant plants in the world. Freesias are grown for ornamental, as well as practical purposes and can be found in a wide variety of colors, as well as varying fragrances - some stronger than others. Freesia flowers are a beautiful bulb flower that is sure to brighten up your garden! It comes in many beautiful colors and has a very sweet fragrance that is sure to be enjoyable!', ['flowers']),
  new Product(4, 'Aster', 5, 5, 'The aster is a flower with a bit of a wild appearance, but it fits nicely in many garden settings. The aster flower is the birth flower for the month of September, and is often used to mark twenty years of marriage.', ['flowers'])
];

export const comments = [
  new Comment(1, 1, '2017-07-01', 'Wilkins', 5, 'This flower looks good'),
  new Comment(2, 1, '2017-07-01', 'Vicky', 5, 'This flower fit for all purposes'),
  new Comment(3, 1, '2017-07-01', 'Jay', 3, 'Color too fade'),
];

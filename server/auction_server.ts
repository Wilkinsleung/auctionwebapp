import * as express from 'express';
import * as bodyParser from 'body-parser';
import { products, comments } from './model';

const server = express();
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.get('/', (req, res) => {
  res.send('Hello World!');
});

// product related API
server.get('/api/products', (req, res) => {
  let params = req.query;
  console.log('/api/product -> filter option: [' + JSON.stringify(params) + ']');
  let result = products;
  if (params.title) {
    result = result.filter(p => p.title.toUpperCase().indexOf(params.title.toUpperCase()) !== -1);
  }

  if (params.price) {
    result = result.filter(p => p.price <= params.price);
  }

  if (params.category && +params.category !== -1) {
    result = result.filter(p => p.category === params.category);
  }
  res.json(result);
});

server.get('/api/product/:id', (req, res) => {
  res.json(
    products.find(product => product.id === +req.params.id)
  );
});

// comment related API
server.get('/api/product/:id/comments', (req, res) => {
  res.json(
    comments.filter(comment => comment.productId === +req.params.id)
  );
});

server.post('/api/product/addcomment', (req, res) => {
  const comment = req.body.comment;
  comments.unshift(comment);
  res.json({
    'status': 200,
    'data': 'ok'
  });
});

server.listen('8085', () => {
  console.log('Auction Server started on : http://localhost:8085');
});

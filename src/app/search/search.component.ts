import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ProductService } from '../shared/services/product.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  formModel: FormGroup;

  constructor(private fb: FormBuilder, private productService: ProductService) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.formModel = this.fb.group({
      title: [null, [Validators.required, Validators.minLength(3)]],
      price: [null, this.positiveNumberValidator],
      category: ['-1'],
    });
  }

  positiveNumberValidator(control: FormControl): any {
    console.log('postivie number validator: ' + control.value);
    if (!control.value) {
      return null;
    }

    if (parseInt(control.value) > 0) {
      return null;
    } else {
      return { positiveNumber: true };
    }
  }

  search() {
    if (this.formModel.valid) {
      this.productService.productSearchEventEmitter.emit(this.formModel.value);
    }
  }
}


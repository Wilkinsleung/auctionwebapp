import { Injectable } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { Observable } from 'rxjs/Rx';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authenticateService: AuthenticateService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    let url: string = state.url;


    return this.checkLogin(url);
  }

  private checkLogin(url: string): boolean {
    if (this.authenticateService.getLoginStatus()) {
      return true;
    }

    // Store the attempted URL for redirecting
    this.authenticateService.redirectUrl = url;

    // Navigate to the login page with extras
    this.router.navigate(['/']);
    return false;
  }


}

import { Injectable, EventEmitter } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AuthenticateService {

  authenticated: EventEmitter<boolean> = new EventEmitter();
  redirectUrl: string;
  constructor(
    private localStorageService: LocalStorageService
  ) { }

  login(username: string, password: string): boolean {
    if (username === 'admin' && password === 'admin') {
      // we fixed the username and password from here
      // we put in local storage to indicate what was happened
      this.localStorageService.set('logined', true);
      this.authenticated.emit(true);
      return true;
    } else {
      return false;
    }
  }

  logout(): boolean {
    this.authenticated.emit(false);
    return this.localStorageService.remove('logined');
  }

  getLoginStatus(): boolean {
    return this.localStorageService.get('logined') ? true : false;
  }
}

import { Product, Comment, products, comments } from './../model/model';
import { Injectable, EventEmitter } from '@angular/core';
import { SearchComponent } from '../../search/search.component';
import { Observable } from 'rxjs/Rx';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ProductService {

  productSearchEventEmitter: EventEmitter<ProductSearchParams> = new EventEmitter();
  private headers = new Headers({ 'Content-Type': 'application/json' });
  constructor(private http: Http) { }

  getProductList(): Observable<Product[]> {
    return this.http.get('/api/products').map(res => res.json());
  }

  getProduct(id: number): Observable<Product> {
    return this.http.get('/api/product/' + id).map(res => res.json());
  }

  getCommentsByProduct(id: number): Observable<Comment[]> {
    return this.http.get('/api/product/' + id + '/comments').map(res => res.json());
  }

  addCommentToByProductId(newcomment: Comment): Observable<any> {
    return this.http.post('/api/product/addcomment', { comment: newcomment }, { headers: this.headers }).map(res => res.json());

  }

  search(params: ProductSearchParams): Observable<Product[]> {
    return this.http.get('/api/products', { search: this.encodeParams(params) }).map(res => res.json());
  }

  private encodeParams(searchParams: ProductSearchParams): URLSearchParams {
    return Object.keys(searchParams).filter(key => searchParams[key]).reduce(
      (sum: URLSearchParams, key: string) => {
        sum.append(key, searchParams[key]);
        return sum;
      }, new URLSearchParams());
  }
}

export class ProductSearchParams {
  constructor(public title: string, public price: number, public category: string) { }
}

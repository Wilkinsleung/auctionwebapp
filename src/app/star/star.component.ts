import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnInit, OnChanges {


  stars: boolean[];

  @Input()
  rating: number = 0;

  @Input()
  disabledStarRating: boolean = false;

  // we can bind this to @Input rating main because our name convension, otherwise a new method is needed to bind
  // we have to specify the xxx($event) on parent view to receive the changes
  @Output()
  ratingChange: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    // we repopulate star on data change!
    // console.log('ngOnChanges' + this.rating);
    this.populateStar();
  }

  populateStar() {
    // we define a list of rating star
    this.stars = [];
    for (let i = 1; i <= 5; i++) {
      this.stars.push(this.rating < i);
    }
  }

  onStarRating(index: number) {
    if (!this.disabledStarRating) {
      this.rating = index + 1;
      // then we repopulate star
      // we change emit our new rating to who is listening on this
      this.ratingChange.emit(this.rating);
      this.populateStar();
    }

  }

}

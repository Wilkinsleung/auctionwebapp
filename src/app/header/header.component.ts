import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../shared/services/authenticate.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  logined: boolean = false;
  formModel: FormGroup;
  constructor(private authenticateService: AuthenticateService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.authenticateService.authenticated.subscribe(logined => this.logined = logined);
    this.logined = this.authenticateService.getLoginStatus();
  }

  login() {
    if (this.formModel.valid) {
      console.log('login clicked from header');
      console.log(this.formModel);
      this.logined = this.authenticateService.login(this.formModel.value['username'], this.formModel.value['password']);
      if(this.logined){
        $('#loginModal').modal('toggle');
      } else {
        // var $dismissBtn = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        // $dismissBtn.appendTo($('#errorMsg'));
        $('#errorMsg').text('this is auto set!').show();
        setTimeout(()=>{
          $('#errorMsg').text('this is auto set!').hide();
        },2500);
      }
    }
  }

  logout() {
    this.authenticateService.logout();
  }

  createForm() {
    this.formModel = this.fb.group({
      'username': [null, [Validators.required, Validators.minLength(5)]],
      'password': [Validators.required, Validators.minLength(5)]
    })
  }

}

import { ProductService } from './../../shared/services/product.service';
import { Component, OnInit } from '@angular/core';
import { Product, products } from '../../shared/model/model';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products$: Observable<Product[]>;
  imageUrl = 'http://placehold.it/320x150';

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.getProductList();
    this.productService.productSearchEventEmitter.subscribe(
      params => {
        this.products$ = this.productService.search(params);
        console.log(this.products$);
      }
    );
  }

}

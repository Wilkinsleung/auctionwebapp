import { Product, Comment } from './../../shared/model/model';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Route, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../shared/services/product.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit, OnChanges {

  product: Product;
  commentList: Comment[];
  isCommentHidden: boolean = false;

  // star component related
  newCommentRating: number = 5;
  newComment: string = '';

  constructor(private routerInfo: ActivatedRoute,
    private productService: ProductService,
    private location: Location) { }

  ngOnInit() {
    let productId = this.routerInfo.snapshot.params['id'];
    this.productService.getProduct(productId).subscribe(
      product => this.product = product
    );
    this.productService.getCommentsByProduct(productId).subscribe(comments => this.commentList = comments);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('im changing');
  }


  addComment() {
    console.log(this.newComment);
    console.log(this.newCommentRating);
    // we put the comment into our product
    let comment = new Comment(0, this.product.id, new Date().toISOString(), 'Wilkins', this.newCommentRating, this.newComment);
    // this.commentList.unshift(comment);
    this.productService.addCommentToByProductId(comment).subscribe(res => {
      if (res.status === 200) {
        // we should be safe to update
        this.commentList.unshift(comment);
      }
    });
    // we recalc the overall product rating
    const overallrating: number = this.commentList.reduce((sum, comment) => {
      return sum += comment.rating;
    }, 0);
    this.product.rating = overallrating / this.commentList.length;
    this.isCommentHidden = false;
    this.newComment = '';
  }

  goBack() {
    this.location.back();
  }
}

import { AuctionwebappPage } from './app.po';

describe('auctionwebapp App', () => {
  let page: AuctionwebappPage;

  beforeEach(() => {
    page = new AuctionwebappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
